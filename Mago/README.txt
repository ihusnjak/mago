/*************************************
 **     Mago - Markup Generator     **
 **             ------              **
 **        version: 1.0.rc2        **
 **********************************/

@author     Ivan Hušnjak <ivan@ui-stuff.com>
@website    http://www.ui-stuff.com/projects/mago
@license    http://www.ui-stuff.com/projects/mago/license



/*************************************
 ** 1. Table of contents            **
 ***********************************/
 
1. Table of contents
2. Basics
3. Nesting
4. Alternators
    4.1. ID alternator #
    4.2. Class .
    4.3. Pseudo-class :
    4.4. Attribute []
    4.5. Text literal ""
    4.6. Markup literal ``
    4.7. Iterate *N
    4.8. Random text generator ~N
    4.9. Cling to alternators <>
5. Chaining multiple alternators
6. Examples
    6.1. Basic HTML5 document
    6.2. Odd-even table rows
    6.3. Form elements
    6.4. Mock XML document
    6.5. Using XML namespaces



/*************************************
 ** 2. Basics                       **
 ***********************************/

Mago - Markup Generator is a set of PHP scripts that uses Mago code syntax to
generate markup for HTML or HTML5 documents and very simple XML documents.
Each line of Mago code is interpreted as a command called "node command".
This reduces issues with \n - new line and \r - carriage return on various OS's.
Empty lines are not parsed.
Using tab or 4 space chars to indentify nesting of one node into another.
Tag names are optional but preferable.
Names (tag, attribute, id, class and pseudo-class names) should start with letter [a-z],
and contain letters, digits, - (dash) and _ (underscore) chars.
XML namespaces are not supported - because obvious conflict with pseudo-classes.
All names will be converted to lowercase! DIV -> div, .MyClass -> .myclass


/*************************************
 ** 3. Nesting                      **
 ***********************************/

When writing Mago code you DO NOT define closing tags!!! Mago will generate them accordingly.
To indicate that node is a child of a parent, simply prepend child node command
with 4 spaces or 1 tab further than parent was.
Example:
------------------------------------------------
body
    div
------------------------------------------------

Results in:
------------------------------------------------
<body>
    <div></div>
</body>
------------------------------------------------

Prepending child node command further will place it on a same position:
Example:
------------------------------------------------
body
                div
        div
------------------------------------------------

Results in:
------------------------------------------------
<body>
    <div></div>
    <div></div>
</body>
------------------------------------------------

In this example that won't happen since first div is over second div so second
will get nested in first:
------------------------------------------------
body
        div
                div
------------------------------------------------

Results in:
------------------------------------------------
<body>
    <div>
        <div></div>
    </div>
</body>
------------------------------------------------



/*************************************
 ** 4. Alternators                  **
 ***********************************/
 
Alternators help create more vivid markup faster. This is the true power of Mago.
Writing small code snippets won't save you much time spend of building markup,
but iterations, random text generators, class stack and pseudo-classes will
seriously decrease the need for copy/paste.
They can be standalone or in conjuction with tag name or other alternators.
Some alternators may occure multiple times in one command line. Some alternators
will assume "div" or "input" as tag names.



/*************************************
 ** 4.1. ID alternator #            **
 ***********************************/
 
If not explicitly specified (or using some pseudo-class) it assumes <div> node.
Example:
------------------------------------------------
#wrapper
------------------------------------------------

Results in:
------------------------------------------------
<div id="wrapper"/>
------------------------------------------------



/*************************************
 ** 4.2. Class .                    **
 ***********************************/
 
If not explicitly specified (or using some pseudo-class) it assumes <div> node.
Example:
------------------------------------------------
.red
------------------------------------------------

Results in:
------------------------------------------------
<div class="red"/>
------------------------------------------------

Class stack:
------------------------------------------------
span.red.big.border
------------------------------------------------

Results in:
------------------------------------------------
<span class="red big border"/>
------------------------------------------------



/*************************************
 ** 4.3. Pseudo-class :             **
 ***********************************/

There are few pseudo-class families that affect:
- comments and CDATA:
    :comment - creates a HTML comment - text or literal value and any nested
    node will be wrapped in comments
    :cdata - creates a CDATA section - text or literal value and any nested
    node will be wrapped in CDATA section
- doctypes:
    :html5 - creates a HTML5 doctype
    :xml - creates a XML document tag if no version and encoding provided it
    assumes version="1.0" and encoding="utf-8"
    :xhtml11dtd - creates a XHTML1.1 DTD doctype
    :xhtml11b - creates a XHTML1.1 basic doctype
    :xhtml1s - creates a XHTML1 strict doctype
    :xhtml1t - creates a XHTML1 transitional doctype
    :xhtml1f - creates a XHTML1 frameset doctype
    :html401s - creates a HTML4.01 strict doctype
    :html401t - creates a HTML4.01 transitional doctype
    :html401f - creates a HTML4.01 frameset doctype
- visibility of node:
    :hide - same as attribute style="display: none"
    :show - same as attribute style="display: block"
- form element state:
    :checked - same as attribute checked="checked"
    :disabled - same as attribute disabled="disabled"
    :multiple - same as attribute multiple="multiple"
    :readonly - same as attribute readonly="readonly"
    :selected - same as attribute selected="selected"
    :required - same as HTML5 attribute required="required"
    :autofocus - same as HTML5 attribute autofocus="autofocus"
    :formnovalidate - same as HTML5 attribute formnovalidate="formnovalidate"
    :autocomplete - same as HTML5 attribute autocomplete="on"
    :autocomplete-off - same as HTML5 attribute autocomplete="off"
- form element type, defaults to input tag:
    :text - same as attribute type="text"
    :password - same as attribute type="password"
    :checkbox - same as attribute type="checkbox"
    :radio - same as attribute type="radio"
    :file - same as attribute type="file"
    :image - same as attribute type="image"
    :hidden - same as attribute type="hidden"
    :submit - same as attribute type="submit", supported by button tag
    :reset - same as attribute type="reset", supported by button tag
    :button - same as attribute type="button", supported by button tag
    :date - same as attribute type="date"
    :datetime - same as attribute type="datetime"
    :datetime-local - same as attribute type="datetime-local"
    :color - same as attribute type="color"
    :email - same as attribute type="email"
    :month - same as attribute type="month"
    :number - same as attribute type="number"
    :range - same as attribute type="range"
    :search - same as attribute type="search"
    :tel - same as attribute type="tel"
    :time - same as attribute type="time"
    :url - same as attribute type="url"
    :week - same as attribute type="week"

Example:
------------------------------------------------
:checkbox:disabled
------------------------------------------------

Results in:
------------------------------------------------
<input type="checkbox" disabled="disabled"/>
------------------------------------------------

Pseudo-class overrides explicit attributes!



/*************************************
 ** 4.4. Attribute []              **
 ***********************************/

Defined in form [attrib1="value1"attrib2="value2"...attribN="valueN"]
" (double quote) inside attribute values must be escaped with \"
Same attribute may occure more than once but will override previous value!
Example:
------------------------------------------------
link[ref="stylesheet"href="style.css"type="text/css"]
------------------------------------------------

Results in:
------------------------------------------------
<link ref="stylesheet" href="style.css" type="text/css"/>
------------------------------------------------



/*************************************
 ** 4.5. Text literal ""            **
 ***********************************/

Inside "" (double quotes) you can place any text except for new lines for which you should use \n and \r escape sequences.
To use quotes inside literal you must escape it with \"
Example:
------------------------------------------------
p"One paragraph \"of\" text with a \nnew line"
------------------------------------------------

Results in:
------------------------------------------------
<p>One paragraph &quot;of&quot; text with a
new line</p>
------------------------------------------------

As you can see this is not really easier nor faster than writting markup yourself - but that is not the purpose of literals.
Note that html markup chars (" & < and >) get replaced by HTML entities.

Example with iterator:
------------------------------------------------
p
    "multiply text"*3
------------------------------------------------

Results in:
------------------------------------------------
<p>
    multiply text
    multiply text
    multiply text
</p>
------------------------------------------------



/*************************************
 ** 4.6. Markup literal ``          **
 ***********************************/

Inside `` (backticks) you can place any text except for new lines for which you should use \n and \r escape sequences.
To use backtics inside literal you must escape it with \`
Quotes are safe to use inside markup literal. Also html special chars are not converted to html entities.
This is usefull to create DOCTYPE definition or nodes containing XML namespaces
Example:
------------------------------------------------
p`snippet of <span style="color:red">HTML</span> gotten from c/p`
------------------------------------------------

Results in:
------------------------------------------------
<p>snippet of <span style="color:red">HTML</span> gotten from c/p</p>
------------------------------------------------



/*************************************
 ** 4.7. Iterate *N                 **
 ***********************************/

Allows iterating node command N times.
Using form *N,M makes it possible to iterate randomly between N (min) and M (max) times.
If node that is being iterated has any children, those will be iterated along with parent.
Example of single node iteration:
------------------------------------------------
body
    p*3
------------------------------------------------

Results in:
------------------------------------------------
<body>
    <p></p>
    <p></p>
    <p></p>
</body>
------------------------------------------------

Example of multiple nodes iteration:
------------------------------------------------
*4
    div.left
    div.right
------------------------------------------------

Results in:
------------------------------------------------
<div class="left"/>
<div class="right"/>
<div class="left"/>
<div class="right"/>
<div class="left"/>
<div class="right"/>
<div class="left"/>
<div class="right"/>
------------------------------------------------


Example of random iteration:
------------------------------------------------
div
    p*2,5
        a
------------------------------------------------

May result in:
------------------------------------------------
<div>
    <p>
        <a></a>
    </p>
    <p>
        <a></a>
    </p>
    <p>
        <a></a>
    </p>
</div>
------------------------------------------------



/*************************************
 ** 4.8. Random text generator ~N   **
 ***********************************/

Enables generation of "Lorem ipsum" random text from text database.
N marks number of words to generate.
The form ~N! will generate random punctuation as well (text will also end in . ! or ?).
The form ~N,M and ~N,M! will work similarly to random iterator, generating between N (min) and M (max) words.
Example:
------------------------------------------------
p~5
------------------------------------------------

May result in:
------------------------------------------------
<p>Dolor cum consectetuer vivae avet</p>
------------------------------------------------

Example:
------------------------------------------------
p~5,10
------------------------------------------------

May result in:
------------------------------------------------
<p>Dolor cum consectetuer vivae avet apiscum lorem</p>
------------------------------------------------

Example:
------------------------------------------------
p~5,10!
------------------------------------------------

May result in:
------------------------------------------------
<p>Dolor cum consectetuer, vivae avet apiscum lorem!</p>
------------------------------------------------



/*************************************
 ** 4.9. Cluig to alternators <>       **
 ***********************************/

Cling - no blank chars like spaces, tabs and new lines between two nodes.
For cling to left use < - this will remove any blanks between this node and previous sibling or parent opening tag
For cling to right use > - this will remove any blanks between this node and next sibling or parent closing tag

Example:
------------------------------------------------
p
    a<>*3
------------------------------------------------

Results in:
------------------------------------------------
<p><a></a><a></a><a></a></p>
------------------------------------------------



/*************************************
 ** 5. Chaining multiple alternators**
 ***********************************/

You can chain multiple alternators along with node names:
Example:
------------------------------------------------
span.big[style="color:red;"]"Big red text"
------------------------------------------------

Results in:
------------------------------------------------
<span class="big" style="color: red;">Big red text</span>
------------------------------------------------



/*************************************
 ** 6. Examples                     **
 ***********************************/
 
 
/*************************************
 ** 6.1. Basic HTML5 document       **
 ***********************************/

------------------------------------------------
:html
html
    head
        meta[charset="utf-8"]
        title"Basic HTML5 document"
    body
------------------------------------------------

Results in:
------------------------------------------------
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Basic HTML5 document</title>
    </head>
    <body></body>
</html>
------------------------------------------------



/*************************************
 ** 6.2. Odd-even table rows        **
 ***********************************/

------------------------------------------------
table
    *2,5
        tr.odd
            td*3
        tr.even
            td*3
------------------------------------------------

May result in:
------------------------------------------------
<table>
    <tr class="odd">
        <td/>
        <td/>
        <td/>
    </tr>
    <tr class="even">
        <td/>
        <td/>
        <td/>
    </tr>
    <tr class="odd">
        <td/>
        <td/>
        <td/>
    </tr>
    <tr class="even">
        <td/>
        <td/>
        <td/>
    </tr>
    <tr class="odd">
        <td/>
        <td/>
        <td/>
    </tr>
    <tr class="even">
        <td/>
        <td/>
        <td/>
    </tr>
</table>
------------------------------------------------



/*************************************
 ** 6.3. Form elements              **
 ***********************************/

This is a quite complex form made by Mago:
------------------------------------------------
form[method="post"action=""]
    fieldset
        legend"Fill out this form"
        label[for="firstname"]"First name:"
        :text#firstname:required
        br
        :checkbox#newsletter:checked
        label[for="newsletter"]"Do you want to recieve our newsletter?"
        br
        :radio[name="radio"value="1"]#radio_1
        label[for="radio_1"]"Radio 1"
        br
        :radio[name="radio"value="2"]#radio_2
        label[for="radio_2"]"Radio 2"
        br
        :radio[name="radio"value="3"]#radio_3
        label[for="radio_3"]"Radio 3"
        br
        label[for="gender"]"Gender:"
        select#gender
            option[value="m"]:selected"Male"
            option[value="f"]"Female"
        br
        label[for="resume"]"Enter your resume:"
        textarea#resume[rows="5"cols="80"]:required
        br
        :reset[value="Clear"]
        :submit[value="Submit form"]
------------------------------------------------

Results in:
------------------------------------------------
<form method="post" action="">
    <fieldset>
        <legend>Fill out this form</legend>
        <label for="firstname">First name:</label>
        <input type="text" id="firstname" required="required"/>
        <br/>
        <input type="checkbox" id="newsletter" checked="checked"/>
        <label for="newsletter">Do you want to recieve our newsletter?</label>
        <br/>
        <input type="radio" name="radio" id="radio_1" value="1"/>
        <label for="radio_1">Radio 1</label>
        <br/>
        <input type="radio" name="radio" id="radio_2" value="2"/>
        <label for="radio_2">Radio 2</label>
        <br/>
        <input type="radio" name="radio" id="radio_3" value="3"/>
        <label for="radio_3">Radio 3</label>
        <br/>
        <label for="gender">Gender:</label>
        <select id="gender">
            <option value="m" selected="selected">Male</option>
            <option value="f">Female</option>
        </select>
        <br/>
        <label for="resume">Enter your resume:</label>
        <textarea id="resume" rows="5" cols="80"></textarea>
        <br/>
        <input type="reset" value="Clear"/>
        <input type="submit" value="Submit form"/>
    </fieldset>
</form>
------------------------------------------------



/*************************************
 ** 6.4. Mock XML document          **
 ***********************************/

------------------------------------------------
:xml
data
    item[id="#"]*3
        product[productId="#"]
            nameEN
                :cdata~1,5
            shortEN
                :cdata~15,50!
            descriptionEN
                :cdata`<p>literal html inside CDATA section</p>`
            price"1234.56"
            quantity"99"
------------------------------------------------

May result in (HTML mode):
------------------------------------------------
<?xml version="1.0" encoding="utf-8"?>
<data>
    <item id="#">
        <product productid="#">
            <nameen><![CDATA[Vel]]></nameen>
            <shorten><![CDATA[Immitto caecus capto acsi odio illum: probo
            letalis typicus dignissim saepius os lenis: camur dignissim elit
            jus demoveo iusto. Nimis ille vereor refero os, sed saepius et
            praesent feugiat, mos utrum lobortis abigo imputo, consectetuer
            loquor exerci validus: premo incassum letatio melior capto odio,
            abico fatua facilisis cui.]]></shorten>
            <descriptionen><![CDATA[<p>literal html inside CDATA
            section</p>]]></descriptionen>
            <price>1234.56</price>
            <quantity>99</quantity>
        </product>
    </item>
    <item id="#">
        <product productid="#">
            <nameen><![CDATA[Iaceo luptatum verto cui]]></nameen>
            <shorten><![CDATA[Hendrerit ludus camur lobortis appellatio: vel
            vero saepius vulpes? Mara si tum neque iustum nostrud neque.
            Rusticus meus plaga causa: quidem huic opto acsi laoreet
            lucidus?]]></shorten>
            <descriptionen><![CDATA[<p>literal html inside CDATA
            section</p>]]></descriptionen>
            <price>1234.56</price>
            <quantity>99</quantity>
        </product>
    </item>
    <item id="#">
        <product productid="#">
            <nameen><![CDATA[Haero lenis in huic]]></nameen>
            <shorten><![CDATA[Defui persto premo exerci utrum decet premo
            nostrud? Appellatio pagus capto fatua loquor. Utrum sudo in
            ulciscor ut dignissim blandit! Secundum occuro magna facilisi,
            delenit antehabeo nimis persto.]]></shorten>
            <descriptionen><![CDATA[<p>literal html inside CDATA
            section</p>]]></descriptionen>
            <price>1234.56</price>
            <quantity>99</quantity>
        </product>
    </item>
</data>
------------------------------------------------

May result in (XML mode):
------------------------------------------------
<?xml version="1.0" encoding="utf-8"?>
<data>
    <item id="#">
        <product productId="#">
            <nameEN><![CDATA[Volutpat]]></nameEN>
            <shortEN><![CDATA[Nutus in letalis scisco nutus: loquor tum lucidus
            foras si quidne. Rusticus humo quidne ipsum loquor foras, caecus
            imputo demoveo demoveo luctus accumsan!]]></shortEN>
            <descriptionEN><![CDATA[<p>literal html inside CDATA
            section</p>]]></descriptionEN>
            <price>1234.56</price>
            <quantity>99</quantity>
        </product>
    </item>
    <item id="#">
        <product productId="#">
            <nameEN><![CDATA[Vero singularis]]></nameEN>
            <shortEN><![CDATA[Wisi obruo ideo opes hendrerit. Bis abico iaceo
            ea distineo erat? Enim brevitas damnum saluto aliquam velit feugiat.
            Caecus saluto damnum velit melior esse: valde verto blandit defui
            sudo nisl. Dolor regula praesent saluto imputo! Comis abico venio
            aliquam lenis letalis.]]></shortEN>
            <descriptionEN><![CDATA[<p>literal html inside CDATA
            section</p>]]></descriptionEN>
            <price>1234.56</price>
            <quantity>99</quantity>
        </product>
    </item>
    <item id="#">
        <product productId="#">
            <nameEN><![CDATA[Esse utrum]]></nameEN>
            <shortEN><![CDATA[Si ibidem eligo ibidem esse interdico probo vel?
            Nobis voco opto nobis erat ideo. Esse diam causa mara?]]></shortEN>
            <descriptionEN><![CDATA[<p>literal html inside CDATA
            section</p>]]></descriptionEN>
            <price>1234.56</price>
            <quantity>99</quantity>
        </product>
    </item>
</data>
------------------------------------------------

Note the difference when using MAGO in XML vs HTML mode.



/*************************************
 ** 6.5. Using XML namespaces       **
 ***********************************/

------------------------------------------------
:html5
`<html xmlns:lang="de">`
    head
    body
`</html>`
------------------------------------------------

Results in:
------------------------------------------------
<!DOCTYPE html>
<html xmlns:lang="de">
<head></head>
<body></body>
</html>
------------------------------------------------

Note that if using markup literals, you must close opened tags yourself!





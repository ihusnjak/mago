<?php
/**
 * Mago - Markup Generator
 * 
 * Parser definition.
 * It is defined abstract since no planns are made to make it very flexible regarding
 * its lexing and analysing behaviour.
 * Perhaps in future there may be a XML parser, HTML and HTML5 parsers available.
 * But then entire structure of this class will be reshapen: lexer and analyzer will be separated etc..
 *
 * @package Mago
 * @author Ivan Hušnjak <ivan@ui-stuff.com>
 * @license http://www.ui-stuff.com/projects/mago/license
 * @version 1.0.rc2
 */

require_once __DIR__ . '/Parser/Exception.php';
require_once __DIR__ . '/TextGenerator.php';
require_once __DIR__ . '/Node.php';

abstract class Mago_Parser
{
    const MODE_HTML = 'html';
    const MODE_XML = 'xml';
    /**
     * Parse Mago code, analyze it for syntax errors and return array of nodes
     * to be processed by renderer.
     *
     * @param string $code
     * @param string $mode Parsing mode
     * @return array
     */
    public static function parse($code, $mode = self::MODE_HTML)
    {
        mb_internal_encoding('utf-8'); // interpret all a utf-8 so mb_* functions can work no problem
        $lines = mb_split("[\n|\r]+", $code); // make no difference between Win and *nx systems
        $lineCount = count($lines);
        $char = 1;
        $nodes = array();
        for ($line = 1; $line <= $lineCount; $line++) { // interpret each line as a node command
            //----×----×----×----×----×----×----×----×----//
            // dragons be here!!!!
            // enter at your own risk
            //----×----×----×----×----×----×----×----×----//
            $level =
                $_empty = 0;
            $iterateMin =
                $iterateMax =
                $attribName =
                $attribValue =
                $class =
                $id =
                $pseudo =
                $literal =
                $randomMin =
                $randomMax =
                $tagName =
                $nodeType = '';
            $classes =
                $pseudos =
                $attribs =
                $literals = array();
            $clingAfter =
                $clingBefore =
                $randomPunct = false;
            $_isEmpty = true;
            $_isIterate =
                $_isAttribs =
                $_isAttribValue =
                $_isTag =
                $_isId =
                $_isClass =
                $_isPseudo =
                $_isText =
                $_isMarkup =
                $_isRandom =
                $_isEscape =
                $_findEnd =
                $_goForMax = false;
            $l = $lines[$line - 1];
            $charCount = mb_strlen($l);
            for ($char = 1; $char <= $charCount; $char++) { // process char by char
                $c = mb_substr($l, $char - 1, 1);
                if ($_isEmpty) { // if still empty search for command or mark level
                    if ($c == ' ') {
                        $_empty ++;
                    } elseif ($c == "\t") {
                        $_empty += 4;
                    } else { // it should be a command
                        $_isEmpty = false;
                        $level = ceil($_empty / 4);
                        $C = mb_convert_case($c, MB_CASE_LOWER);
                        if ($c == '*') { // it is an iterate notation
                            $_isIterate = true;
                        } elseif ($c == '<') {
                            $clingBefore = true;
                        } elseif ($c == '>') {
                            $clingAfter = true;
                        } elseif ($c == '[') {
                            $_isAttribs = true;
                        } elseif ($c == '.') {
                            $_isClass = true;
                        } elseif ($c == '#') {
                            $_isId = true;
                        } elseif ($c == ':') {
                            $_isPseudo = true;
                        } elseif ($c == '"') {
                            $_isText = true;
                        } elseif ($c == '`') {
                            $_isMarkup = true;
                        } elseif ($c == '~') { // it is a random text generator notation
                            $_isRandom = true;
                        } elseif ($C >= 'a' and $C <= 'z') {
                            $_isTag = true;
                            $tagName .= ($mode == self::MODE_XML ? $c : $C);
                        } else {
                            throw new Mago_Parser_Exception(
                                "Unsupported char {$c} found at line {$line} char {$char}"
                            );
                        }
                    }
                } elseif ($_isIterate) { // search for number after *
                    if(!$c and $c!='0' and $iterateMin == '') {
                        throw new Mago_Parser_Exception(
                            "Expecting number of iterations at line {$line} char {$char}"
                        );
                    } elseif (is_numeric($c)) { // 0..9
                        if ($_goForMax) {
                            $iterateMax .= $c;
                        } else {
                            $iterateMin .= $c;
                        }
                    } elseif ($c == ',') { // go for max?
                        if ($_goForMax) {
                            throw new Mago_Parser_Exception(
                                "Max number of iterations already specified at line {$line} char {$char}"
                            );
                        }
                        $_goForMax = true;
                    } else {
                        $_isIterate = $_goForMax = false;
                        $char--; // return 1 step back
                    }
                } elseif ($_isRandom) { // search for number after ~
                    if(!$c and $c!='0' and $randomMin == '') {
                        throw new Mago_Parser_Exception(
                            "Expecting min number of words to generate at line {$line} char {$char}"
                        );
                    } elseif (is_numeric($c)) { // 0..9
                        if ($_goForMax) {
                            $randomMax .= $c;
                        } else {
                            $randomMin .= $c;
                        }
                    } elseif ($c == ',') { // go for max?
                        if ($_goForMax) {
                            throw new Mago_Parser_Exception(
                                "Max number of words already specified at line {$line} char {$char}"
                            );
                        }
                        $_goForMax = true;
                    } else {
                        if ($c == '!') {
                            $randomPunct = true;
                        } else {
                            $char--; // return 1 step back
                        }
                        $_isRandom = $_goForMax = false;
                        $literals[] = array('random', $randomMin, $randomMax, $randomPunct);
                        $randomMin = $randomMax = '';
                        $randomPunct = false;
                    }
                } elseif ($_isText) { // put everything in literal unless " found that is not escaped
                    if ($literal == '') {
                        if ($c == '"') { // literal notation found
                            if (!$_findEnd) {
                                $_findEnd = true;
                            } elseif ($_isEscape) {
                                $literal = mb_substr($literal, 0, -1);
                                die('WFT');
                                $literal .= $c;
                                $_isEscape = false;
                            } else { // literal ended
                                $literals[] = array('text', $literal);
                                $literal = '';
                                $_isText = $_findEnd = false;
                            }
                        } elseif (!$_isEscape and $c == '\\') {
                            $_isEscape = true;
                            $literal .= $c;
                        } else {
                            $literal .= $c;
                            $_isEscape = false;
                        }
                    } else {
                        if ($c == '"') { // literal notation found
                            if ($_isEscape) {
                                $literal = mb_substr($literal, 0, -1);
                                $literal .= $c;
                                $_isEscape = false;
                            } else { // literal ended
                                $literals[] = array('text', $literal);
                                $literal = '';
                                $_isText = $_findEnd = false;
                            }
                        } elseif (!$_isEscape and $c == '\\') {
                            $_isEscape = true;
                            $literal .= $c;
                        } else {
                            $literal .= $c;
                            $_isEscape = false;
                        }
                    }
                } elseif ($_isMarkup) { // put everything in literal unless ` found that is not escaped
                    if ($literal == '') {
                        if ($c == '`') { // literal notation found
                            if (!$_findEnd) {
                                $_findEnd = true;
                            } elseif ($_isEscape) {
                                $literal = mb_substr($literal, 0, -1);
                                $literal .= $c;
                                $_isEscape = false;
                            } else { // literal ended
                                $literals[] = array('markup', $literal);
                                $literal = '';
                                $_isMarkup = $_findEnd = false;
                            }
                        } elseif (!$_isEscape and $c == '\\') {
                            $_isEscape = true;
                            $literal .= $c;
                        } else {
                            $literal .= $c;
                            $_isEscape = false;
                        }
                    } else {
                        if ($c == '`') { // literal notation found
                            if ($_isEscape) {
                                $literal = mb_substr($literal, 0, -1);
                                $literal .= $c;
                                $_isEscape = false;
                            } else { // literal ended
                                $literals[] = array('markup', $literal);
                                $literal = '';
                                $_isMarkup = $_findEnd = false;
                            }
                        } elseif (!$_isEscape and $c == '\\') {
                            $_isEscape = true;
                            $literal .= $c;
                        } else {
                            $literal .= $c;
                            $_isEscape = false;
                        }
                    }
                } elseif ($_isClass or $_isTag or $_isPseudo or $_isId) { // class, id, tag name and pseudo-class are similar
                    $C = mb_convert_case($c, MB_CASE_LOWER);
                    $v1 = $_isClass? '_isClass' : ($_isTag? '_isTag': ($_isId? '_isId': '_isPseudo'));
                    $v2 = $_isClass? 'class' : ($_isTag? 'tagName': ($_isId? 'id': 'pseudo'));
                    if ($C >= 'a' and $C <= 'z'){ // contains a-z
                        $$v2 .= (($_isTag and $mode == self::MODE_XML) ? $c : $C);
                    } elseif( is_numeric($c) or $c == '-' or $c == '_') { // contains 0-9 - or _ that is not at 1st place
                        if (!$$v2) {
                            throw new Mago_Parser_Exception(
                                "Expecting letter, found \"{$c}\" instead at line {$line} char {$char}"
                            );
                        }
                        $$v2 .= $c;
                    } else {
                        if ($_isClass) {
                            $classes[$class] = $class; // save
                            $class = ''; // clear
                        } elseif ($_isPseudo) {
                            $pseudos[$pseudo] = $pseudo; // save
                            $pseudo = ''; // clear
                        }
                        $$v1 = false;
                        $char--; // return 1 step back
                    }
                } elseif ($_isAttribs) { // go for attributes
                    $C = mb_convert_case($c, MB_CASE_LOWER);
                    if (!$attribName) { // search for attrib name
                        if ($C >= 'a' and $C <= 'z'){ // first char catched
                            $attribName .= ($mode == self::MODE_XML ? $c : $C);
                        } elseif (!$_isAttribValue and $c == ']') { // if closing found - no attributes specified
                            $_isAttribs = false;
                        } else {
                            throw new Mago_Parser_Exception(
                                "Expecting attribute name, found \"{$c}\" instead at line {$line} char {$char}"
                            );
                        } 
                    } elseif ($_isAttribValue) {
                        if ($_isEscape) { // catch \
                            if ($c == '"') {
                                $attribValue = mb_substr($attribValue, 0, -1);
                            }
                            $attribValue .= $c;
                            $_isEscape = false;
                        } elseif ($c == '\\') {
                            $_isEscape = true;
                            $attribValue .= $c;
                        } elseif ($c == '"') { // close attribute value
                            $_isAttribValue = false;
                            $attribs[$attribName] = $attribValue; // save
                            $attribName = $attribValue = ''; // clean
                        } else {
                            $attribValue .= $c;
                        }
                    } elseif(($C >= 'a' and $C <= 'z') or is_numeric($c) or $c == '-' or $c == '_') { // new attrib name char
                        $attribName .= ($mode == self::MODE_XML ? $c : $C);
                    } elseif (!$_isAttribValue and $c == '=') {
                        $c = mb_substr($l, $char, 1);
                        $char ++;
                        if ($c == '"') {
                            $_isAttribValue = true;
                        } else {
                            throw new Mago_Parser_Exception(
                                "Expecting \" to indicate attribute value, found \"{$c}\" instead at line {$line} char {$char}"
                            );
                        }
                    } else {
                        throw new Mago_Parser_Exception(
                            "\"{$c}\" is not a proper attribute name char, found at line {$line} char {$char}"
                        );
                    }
                } else { // previous command finished - determine next command
                    $C = mb_convert_case($c, MB_CASE_LOWER);
                    // here resides most of analysis logic
                    if($c == ' ' or $c == "\t") {
                        // continue
                    } elseif ($iterateMin != '' and $c == '*') {
                        throw new Mago_Parser_Exception(
                            "Another iteration alternator found at line {$line} char {$char}"
                        );
                    } elseif ($c == '*') { // it is an iterate notation
                        $_isIterate = true;
                    } elseif ($randomMin != '' and $c == '~') {
                        throw new Mago_Parser_Exception(
                            "Another word generator found at line {$line} char {$char}"
                        );
                    } elseif ($c == '~') { // it is a random text generator notation
                        $_isRandom = true;
                    } elseif ($clingBefore and $c == '<') {
                        throw new Mago_Parser_Exception(
                            "Another cling before alternator found at line {$line} char {$char}"
                        );
                    } elseif ($c == '<') {
                        $clingBefore = true;
                    } elseif ($clingAfter and $c == '>') {
                        throw new Mago_Parser_Exception(
                            "Another cling after alternator found at line {$line} char {$char}"
                        );
                    } elseif ($c == '>') {
                        $clingAfter = true;
                    } elseif ($attribs and $c == '[') {
                        throw new Mago_Parser_Exception(
                            "Another attributes alternator found at line {$line} char {$char}"
                        );
                    } elseif ($c == '[') {
                        $_isAttribs = true;
                    } elseif ($c == '.') { // classes may be defined multiple times
                        $_isClass = true;
                    } elseif ($id and $c == '#') {
                        throw new Mago_Parser_Exception(
                            "Another id alternator found at line {$line} char {$char}"
                        );
                    } elseif ($c == '#') {
                        $_isId = true;
                    } elseif ($c == ':') {
                        $_isPseudo = true;
                    } elseif ($c == '"') {
                        $_isText = true;
                    } elseif ($c == '`') {
                        $_isMarkup = true;
                    } elseif ($tagName and ($C >= 'a' and $C <= 'z')) {
                        throw new Mago_Parser_Exception(
                            "Another tag name found at line {$line} char {$char}"
                        );
                    } elseif ($C >= 'a' and $C <= 'z') {
                        $_isTag = true;
                        $tagName .= ($mode == self::MODE_XML ? $c : $C);
                    } else {
                        throw new Mago_Parser_Exception(
                            "Unsupported char {$c} found at line {$line} char {$char}"
                        );
                    }
                }
            }
            if ($_isMarkup or $_isText) { // if markup or text not finished in this line
                throw new Mago_Parser_Exception(
                    $_isText ?
                      "Line end reached without text literal end \" character found / at line {$line} char {$char}"
                    : "Line end reached without markup literal end ` character found / at line {$line} char {$char}"
                );
            }
            // finish all unclosed commands
            if ($class) {
                $classes[$class] = $class;
            }
            if ($pseudo) {
                $pseudos[$pseudo] = $pseudo;
            }
            if ($randomMin != '') {
                $literals[] = array('random', $randomMin, $randomMax, $randomPunct);
            }
            // line is parsed, now setup a node
            $node = new Mago_Node();
            $node->classes = $classes;
            if (isset($attribs['class'])) { // tidy up attribute classes so direct . notations can override it
                $classes = mb_split('\s+', trim(mb_convert_case($attribs['class'], MB_CASE_LOWER)));
                foreach ($classes as $class) {
                    $node->classes[$class] = $class;
                }
                unset($attribs['class']);
            }
            if (isset($attribs['style'])) { // tidy up styles - remove duplicates, fix property name casings
                // also allow pseudo-classes :hide and :show to manipulate previous values
                $styles = mb_split(';+', trim($attribs['style'], ';'));
                $attribs['style'] = array();
                foreach ($styles as $style) {
                    list($key, $value) = explode(':', $style, 2);
                    $key = mb_convert_case(trim($key), MB_CASE_LOWER); // property names are lowercased!
                    $attribs['style'][$key] = trim($value);
                }
            }
            foreach ($pseudos as $pseudo) { // process pseudo-classes
                $button = false;
                switch ($pseudo) {
                    case 'html5':
                        $node->content = '<!DOCTYPE html>';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'xhtml11dtd':
                        $node->content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
   "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'xhtml11b':
                        $node->content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'xhtml1s':
                        $node->content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'xhtml1t':
                        $node->content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'xhtml1f':
                        $node->content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'html401s':
                        $node->content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'html401t':
                        $node->content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'html401f':
                        $node->content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
   "http://www.w3.org/TR/html4/frameset.dtd">';
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_DOCTYPE; break;
                    case 'xml':
                        if (!isset($attribs['version'])) {
                            $attribs['version'] = '1.0';
                        }
                        if (!isset($attribs['encoding'])) {
                            $attribs['encoding'] = 'utf-8';
                        }
                        $literals = array();
                        $node->nodeType = Mago_Node::TYPE_XML; break;
                    case 'comment':
                        $node->nodeType = Mago_Node::TYPE_COMMENT; break;
                    case 'cdata':
                        // CDATA always uses cling to
                        $clingAfter = true;
                        $clingBefore = true;
                        $node->nodeType = Mago_Node::TYPE_CDATA; break;
                    case 'hide':
                        $attribs['style']['display'] = 'none'; break;
                    case 'show':
                        $attribs['style']['display'] = 'block'; break;
                    case 'multiple':
                    case 'checked':
                    case 'selected':
                    case 'disabled':
                    case 'readonly':
                    // new html5 attributes
                    case 'required':
                    case 'autofocus':
                    case 'formnovalidate':
                        $attribs[$pseudo] = $pseudo;
                        break;
                    case 'autocomlete':
                        $attribs[$pseudo] = 'on';
                        break;
                    case 'autocomlete-off':
                        $attribs[$pseudo] = 'off';
                        break;
                    case 'button':
                    case 'submit':
                    case 'reset':
                        if ($tagName and $tagName != 'input' and $tagName != 'button') {
                            throw new Mago_Parser_Exception(
                                "Using pseudo alternator \"{$pseudo}\" implies tag name to be \"input\" or \"button\" found at line {$line} char {$char}"
                            );
                        }
                        $button = true;
                    case 'text':
                    case 'radio':
                    case 'checkbox':
                    case 'file':
                    case 'password':
                    case 'hidden':
                    case 'image':
                    // new html5 types
                    case 'color':
                    case 'date':
                    case 'datetime':
                    case 'datetime-local':
                    case 'email':
                    case 'month':
                    case 'number':
                    case 'range':
                    case 'search':
                    case 'tel':
                    case 'time':
                    case 'url':
                    case 'week':
                        if (!$button and $tagName and $tagName != 'input') {
                            throw new Mago_Parser_Exception(
                                "Using pseudo alternator \"{$pseudo}\" implies tag name to be \"input\" found at line {$line} char {$char}"
                            );
                        }
                        if (!$tagName) {
                            $tagName = 'input';
                        }
                        $attribs['type'] = $pseudo;
                }
            }
            
            if ($tagName) { // or leave default "div"
                $node->tagName = $tagName;
            }
            if (isset($attribs['style'])) { // now that pseudo-classes are done, combine style attribute back together
                $o = array();
                foreach ($attribs['style'] as $key => $value) {
                    $o[] = $key . ': ' . $value;
                }
                $attribs['style'] = implode(';', $o);
            }
            $node->clingAfter = $clingAfter;
            $node->clingBefore = $clingBefore;
            if($iterateMin != ''){ // if explicit iteration defined
                $node->iterateMin = abs((int) $iterateMin);
                $node->iterateMax = abs((int) $iterateMax);
                if($node->iterateMax < $node->iterateMin) {
                    $node->iterateMax = $node->iterateMin;
                }
            }
            $node->line = $line;
            if ($id) { // combine id or override previous value in attributes
                $attribs['id'] = $id;
            }
            $node->attributes = $attribs;
            if (!$tagName and !$id and !$classes and !$pseudos and !$attribs) {
                // if none of these exist then node is only a container for children nodes
                $node->nodeType = Mago_Node::TYPE_CONTAINER;
                $node->tagName = '';
            }
            foreach ($literals as $literal) {
                $childNode = new Mago_Node();
                $childNode->nodeType = $literal[0];
                if ($literal[0] ==  Mago_Node::TYPE_RANDOM) { // random text is interchangeable with other literals but is specific
                    $childNode->randomMin = (int) $literal[1];
                    $childNode->randomMax = (int) $literal[2];
                    $childNode->randomPunct = $literal[3];
                } else {
                    //print_r($literal);
                    //die();
                    $srch = array('\\n', '\\r', '\\t'); // replace escape sequences
                    $repl = array("\n", "\r", "\t"); // for real deals ;)
                    // \" in text and \` in markup literals are already replaced
                    $childNode->content = str_replace($srch, $repl, $literal[1]);
                }
                if ($node->tagName) { // if not in container
                    $childNode->clingAfter = true;
                    $childNode->clingBefore = true;
                }
                $childNode->line = $line;
                $node->childNodes[] = $childNode;
            }
            if(!$level or !$nodes) { // level 0
                $nodes[] = $node;
            } else {
                $parent = self::findParent($nodes, $level);
                $parent->childNodes[] = $node;
            }
        }
        return $nodes;
    }
    /**
     * Find a proper parent for child node.
     * If level is much higher than the last node parent, child will be placed in that parent.
     * For example: last parent is level 4, 1st child is level 6, 2nd child is level 5 - both will become level 5
     *
     * @param array $nodes
     * @param int $level
     * @param int $currentLevel
     * @return Mago_Node
     */
    protected static function findParent($nodes, $level, $currentLevel = 0)
    {
        $last = array_slice($nodes, -1); // take last node
        $last = $last[0];
        $currentLevel++;
        if ($currentLevel == $level) {
            return $last;
        } elseif ($last->childNodes) {
            return self::findParent($last->childNodes, $level, $currentLevel);
        }
        return $last;
    }
}
<?php
/**
 * Mago - Markup Generator
 * 
 * Node object definition.
 * It is basically simple object with only public properties - very specific and narrow usage
 *
 * @package Mago
 * @author Ivan Hušnjak <ivan@ui-stuff.com>
 * @license http://www.ui-stuff.com/projects/mago/license
 * @version 1.0.rc2
 */
class Mago_Node
{
    const TYPE_ELEMENT      = 'element';
    const TYPE_CONTAINER    = 'container';
    const TYPE_TEXT         = 'text';
    const TYPE_MARKUP       = 'markup';
    const TYPE_RANDOM       = 'random';
    const TYPE_COMMENT      = 'comment';
    const TYPE_CDATA        = 'cdata';
    const TYPE_DOCTYPE      = 'doctype';
    const TYPE_XML          = 'xml';
    /**
     * Line where node command was found.
     * @var int
     */
    public $line = 0;
    /**
     * Possible values: element, container, text, markup, random, comment, cdata, doctype, xml.
     * @var string
     */
    public $nodeType = 'element';
    /**
     * Containers have no tag name
     * @var string
     */
    public $tagName = 'div';
    /**
     * Node attributes attribute => value. Classes are separated from other attributes due to their nature.
     * @var array
     */
    public $attributes = array();
    /**
     * Node classes className => className - to suppres same class appearing more than once.
     * @var array
     */
    public $classes = array();
    /**
     * @var array
     */
    public $childNodes = array();
    /**
     * No-blank chars are generated between this node and previous sibling or parent opening tag.
     * @var bool
     */
    public $clingBefore = false;
    /**
     * No-blank chars are generated between this node and next sibling or parent closing tag.
     * @var bool
     */
    public $clingAfter = false;
    /**
     * Node types text and markup use this property to specify literal content.
     * @var string
     */
    public $content = '';
    /**
     * Iterate 0 means node and none of its children get generated!
     * @var int
     */
    public $iterateMin = 1;
    /**
     * Max must be larger than min!
     * @var int
     */
    public $iterateMax = 1;
    /**
     * Used by node type random
     * @var int
     */
    public $randomMin = 0;
    /**
     * Used by node type random - 0 or less than randomMin implies that exactly randomMin random words will get generated.
     * @var int
     */
    public $randomMax = 0;
    /**
     * Used by node type random to generate random punctuation.
     * @var bool
     */
    public $randomPunct = false;
}

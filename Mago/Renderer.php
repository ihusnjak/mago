<?php
/**
 * Mago - Markup Generator
 * 
 * Renderer object definition.
 *
 * @package Mago
 * @author Ivan Hušnjak <ivan@ui-stuff.com>
 * @license http://www.ui-stuff.com/projects/mago/license
 * @version 1.0.rc2
 */

require_once __DIR__ . '/Exception.php';
require_once __DIR__ . '/Node.php';
require_once __DIR__ . '/Parser.php';

class Mago_Renderer
{
    /**
     * @var array
     */
    protected $nodes;
    /**
     * @var string
     */
    protected $mode = Mago_Parser::MODE_HTML;
    /**
     * @var string
     */
    protected $code;
    /**
     * @var string
     */
    protected $pad = "    ";
    /**
     * @var string
     */
    protected $nl = "\n";
    /**
     * These tag names require having opening and closing tag wether containg child nodes or not.
     * Fixes problems with some browsers intepretation of self enclosed HTML tags.
     * <div/> by HTML5 standard is not proper tag specification and it will break HTML view
     * - it must be <div></div>, same is for <p>, <a>, <textarea> and some other
     * 
     * @var array 
     */
    protected $nodesWithEndTag = array(
        'a',
        'textarea',
        'p',
        'div',
        'span',
        'table',
        'thead',
        'tbody',
        'tfoot',
        'script',
        'style',
        'noscript',
        'body',
        'html',
        'head'
    );
    /**
     * @param string $code Mago code string
     * @param string $mode Rendering mode
     */
    public function __construct($code = NULL, $mode = Mago_Parser::MODE_HTML)
    {
        $this->setCode($code);
        $this->setMode($mode);
    }
    /**
     * Set rendering mode.
     *
     * @param string $mode
     * @return Mago_Renderer
     */
    public function setMode($mode)
    {
        $this->mode = ($mode == Mago_Parser::MODE_XML? Mago_Parser::MODE_XML: Mago_Parser::MODE_HTML);
        return $this;
    }
    /**
     * Set Mago code to process.
     *
     * @param string $code
     * @return Mago_Renderer
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
    /**
     * Set padding string. By default Mago uses 4 spaces to pad output nodes.
     *
     * @param string $pad
     * @return Mago_Renderer
     */
    public function setPad($pad)
    {
        $this->pad = $pad;
        return $this;
    }
    /**
     * Set new line character. By default Mago uses \n.
     *
     * @param string $nl
     * @return Mago_Renderer
     */
    public function setNl($nl)
    {
        $this->nl = $nl;
        return $this;
    }
    /**
     * Process Mago code and generate markup.
     *
     * @param bool $stored Store processed nodes for rendering again or use previously parsed nodes to render again
     * @return string
     */
    public function render($stored = false)
    {
        if (!$this->code) {
            throw new Mago_Exception('Code to generate markup is missing');
        }
        if ($stored) {
            $nodes = $this->nodes;
        }
        if ($nodes === NULL) {
            $nodes = Mago_Parser::parse($this->code, $this->mode);
            if ($stored) {
                $this->nodes = $nodes;
            }
        }
        $output = '';
        foreach ($nodes as $i => $node) {
            $output .= $this->_processNode($node, rand($node->iterateMin, $node->iterateMax), 0, $node->clingAfter);
        }
        return $output;
    }
    /**
     * Clear stored nodes. New call to render() will execute parsing process again.
     *
     * @return Mago_Renderer
     */
    public function clearStored()
    {
        $this->nodes = null;
        return $this;
    }
    /**
     * Helper method to setup single node depending on level and clinging.
     *
     * @param Mago_Node $node
     * @param int $iterate
     * @param int $level Used to generate padding
     * @param bool $clingBefore If previous sibling had clingAfter or parent had cling after
     * @return string
     */
    protected function _processNode(Mago_Node $node, $iterate = 1, $level = 0, $clingBefore = false)
    {
        if(!$iterate) {
            return '';
        }
        $before = $node->clingBefore || $clingBefore;
        if ($node->nodeType == Mago_Node::TYPE_TEXT) {
            $output = $this->_pad($level, $before) . htmlspecialchars($node->content);
        } elseif ($node->nodeType == Mago_Node::TYPE_MARKUP or $node->nodeType == Mago_Node::TYPE_DOCTYPE) {
            $output = $this->_pad($level, $before) . $node->content;
        } elseif ($node->nodeType == Mago_Node::TYPE_RANDOM) {
            $output = $this->_pad($level, $before) .
                Mago_TextGenerator::generate($node->randomMin, $node->randomMax, $node->randomPunct);
        } elseif ($node->nodeType == Mago_Node::TYPE_XML) {
            $output = '<?xml' . $this->_processAttr($node) . '?>';
        } else {
            $output = '';
            $isElement  = $node->nodeType == Mago_Node::TYPE_ELEMENT;
            $isComment  = $node->nodeType == Mago_Node::TYPE_COMMENT;
            $isCdata    = $node->nodeType == Mago_Node::TYPE_CDATA;
            if ($isElement) {
                $output = $this->_pad($level, $before) . '<' . $node->tagName . $this->_processAttr($node);
            } elseif ($isComment) {
                $output = $this->_pad($level, $before) . '<!--';
            } elseif ($isCdata) {
                $output = $this->_pad($level, $before) . '<![CDATA[';
            }
            if ($node->childNodes) {
                if ($isElement) {
                    $output .= '>';
                }
                $after = $node->clingAfter;
                foreach ($node->childNodes as $i => $childNode) {
                    $output .= $this->_processNode($childNode,
                            rand($childNode->iterateMin, $childNode->iterateMax),
                            ($isElement? $level + 1: $level),
                            $after
                        );
                    $after = $childNode->clingAfter;
                }
                $output .= $this->_pad($level, (!$isElement and !$isComment) || $node->clingAfter || $after);
                if ($isElement) {
                    $output .= '</' . $node->tagName . '>';
                } elseif ($isComment) {
                    $output .= '-->';
                } elseif ($isCdata) {
                    $output .= ']]>';
                }
            } elseif ($isElement) {
                if($this->mode == Mago_Parser::MODE_HTML and in_array($node->tagName, $this->nodesWithEndTag)) {
                    $output .= '></' . $node->tagName . '>';
                } else {
                    $output .= '/>';
                }
            } elseif ($isComment) {
                $output .= '-->';
            } elseif ($isCdata) {
                $output .= ']]>';
            }
        }
        if ($iterate > 1) {
            $iterate--; // decrease iteration
            $output .= self::_processNode($node, $iterate, $level, $clingBefore);
        }
        return $output;
    }
    /**
     * Helper method to process node attributes before placing them into opening tag.
     *
     * @param Mago_Node $node
     * @return string
     */
    protected function _processAttr(Mago_Node $node)
    {
        $output = array();
        $attributes = $node->attributes;
        if ($node->classes) {
            $attributes['class'] = implode(' ', $node->classes);
        }
        foreach ($attributes as $key => $value) {
            $output[] = $key . '="' . htmlspecialchars($value) . '"';
        }
        return $attributes ? ' ' . implode(' ', $output) : '';
    }
    /**
     * Helper method that creates new line and padding unless flag is raised.
     *
     * @param int $level
     * @param bool $dont
     * @return string
     */
    protected function _pad($level, $dont = false)
    {
        return ($dont ? '' : $this->nl . str_repeat($this->pad, $level));
    }
}

<?php
/**
 * Mago - Markup Generator
 * 
 * @package Mago
 * @author Ivan Hušnjak <ivan@ui-stuff.com>
 * @license http://www.ui-stuff.com/projects/mago/license
 * @version 1.0.rc2
 */
class Mago_Parser_Exception extends Exception
{
    
}

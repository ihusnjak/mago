<?php
/**
 * Mago - Markup Generator
 * 
 * Generator of random text from presented text database.
 *
 * @package Mago
 * @author Ivan Hušnjak <ivan@ui-stuff.com>
 * @license http://www.ui-stuff.com/projects/mago/license
 * @version 1.0.rc2
 */

require_once __DIR__ . '/TextGenerator/Exception.php';

abstract class Mago_TextGenerator
{
    /**
     * @var array
     */
    protected static $words = array();
    /**
     * @var int
     */
    protected static $count = 0;
    /**
     * Generate random text from presented text database.
     * 
     * @param int $min Minimal number of words generated
     * @param int $max Max number of words, optional, if 0 or less than $min only minimal number of words is generated
     * @param bool $punctuation Random punctuation will be generated as well and text will end in . ! or ?
     * @return string
     */
    public static function generate($min, $max = 0, $punctuation = false)
    {
        if ($min < 0) {
            throw new Mago_TextGenerator_Exception("Expecting a positive number");
        }
        $num = $min; // generate exactly $min words
        if ($max and $max > $min) { // or random number of words
            $num = rand($min, $max);
        }
        if(! $num) { // nothing to generate
            return "";
        }
        self::init(); // only now load words DB
        $start = true;
        $generated = array();
        for ($i = 0; $i < $num; $i++) {
            $index = self::getRandom(self::$count);
            $generated[] = self::$words[$index];
        }
        $generated[0] = ucfirst($generated[0]); // uppercase first
        if ($punctuation) {
            $puncts = ",,,,:...!?"; // give higher chance for , and . than other chars
            $punctsc = strlen($puncts);
            $punctf = "..!.?"; // finishing interpuncts
            $punctfc = strlen($punctf);
            // assume a random punctuation should be generated every 4-7 words
            $next = 4 + self::getRandom(4);
            if ($next < $num - 4) {
                do {
                    // generate random punct
                    $index = self::getRandom($punctsc);
                    $p = $puncts[$index];
                    $generated[$next] .= $p;
                    if (($p == '.' || $p == '!' || $p == '?') and isset($generated[$next + 1])) {
                        $generated[$next + 1] = ucfirst($generated[$next + 1]); // uppercase
                    }
                    $next += 4 + self::getRandom(4); // 4-7
                } while ($next <= $num - 4); // last three leave for finishing
            }
            $generated[$num - 1] .= $punctf[self::getRandom($punctfc)]; // finish with punctuation
        }
        return implode(" ", $generated);
    }
    /**
     * Lazy loader
     * 
     */
    protected static function init()
    {
        if (!self::$words) {
            $db = file_get_contents(__DIR__ . '/TextGenerator/text.db.txt');
            self::$words = explode(' ', $db);
            self::$count = count(self::$words);
        }
    }
    /**
     * Uses much higher entrophy to generate random integer from 0 to max - 1
     *
     * @param int $max
     * @return int
     */
    protected static function getRandom($max)
    {
        return rand(0, 65535) % $max;
    }
}